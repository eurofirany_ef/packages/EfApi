<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'auth_url' => env('EF_AUTH_URL')
];
