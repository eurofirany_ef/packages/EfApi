<?php

namespace Eurofirany\EfApi;
use Illuminate\Support\Facades\Http;

class EfApi
{
    public function checkToken(string $token)
    {
        $efAuthUrl = config('ef_api.auth_url');

        $checkToken = Http::post($efAuthUrl, [
            'token' => $token
        ]);

        if($checkToken->status() == 200) {
            cache()->remember($token, 60 * 60 * 8, function () use ($checkToken) {
                return $checkToken->object()->data;
            });

            return true;
        }

        return false;
    }
}
