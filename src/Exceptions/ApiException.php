<?php

namespace Eurofirany\EfApi\Exceptions;

use Exception;

class ApiException extends Exception
{
    public function render($request)
    {
        if ($request->wantsJson()) {
            return $this->customApiResponse($this);
        } else {
            return response()->json([
                'message' => 'Not Acceptable'
            ], 406);
        }
    }

    private function customApiResponse($exception): \Illuminate\Http\JsonResponse
    {
        if (method_exists($exception, 'getCode')) {
            $statusCode = $exception->getCode();
        } else {
            $statusCode = 500;
        }

        $response = [];

        switch ($statusCode) {
            case 401:
                $response['message'] = $exception->getMessage() ?? 'Unauthorized';
                break;
            case 403:
                $response['message'] = $exception->getMessage() ?? 'Forbidden';
                break;
            case 404:
                $response['message'] = $exception->getMessage() ?? 'Not Found';
                break;
            case 405:
                $response['message'] = $exception->getMessage() ?? 'Method Not Allowed';
                break;
            case 422:
                $response['message'] = $exception->getMessage();
                break;
            default:
                $response['message'] = ($statusCode == 500) ? 'Whoops, looks like something went wrong' : $exception->getMessage();
                break;
        }

        if (config('app.debug')) {
            $response['trace'] = $exception->getTrace();
            $response['code'] = $exception->getCode();
        }

        return response()->json($response, $statusCode);
    }
}
