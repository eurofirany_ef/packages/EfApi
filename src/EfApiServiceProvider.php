<?php

namespace Eurofirany\EfApi;

use Illuminate\Support\ServiceProvider;
use Eurofirany\EfApi\Http\Middleware\EfTokenMiddleware;

class EfApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ef-api');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ef_api.php'),
            ], 'ef_api');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ef_api');

        // Register Middleware
        app('router')->aliasMiddleware('efApi', EfTokenMiddleware::class);

        // Register the main class to use with the facade
        $this->app->singleton('ef-api', function () {
            return new EfApi;
        });
    }
}
