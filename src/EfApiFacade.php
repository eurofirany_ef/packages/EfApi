<?php

namespace Eurofirany\EfApi;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Eurofirany\EfApi\Skeleton\SkeletonClass
 */
class EfApiFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ef-api';
    }
}
