<?php

if (!function_exists('ApiUser')) {
    function ApiUser()
    {
        return cache()->get(request()->bearerToken());
    }
}
