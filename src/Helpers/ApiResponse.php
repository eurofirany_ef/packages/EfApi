<?php

if (!function_exists('ApiResponse')) {
    function ApiResponse($message, $code, $data = null)
    {
        $headers = [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
        ];

        $response['message'] = $message ?? '';

        if($data)
            $response['data'] = $data;

        return response()->json($response, $code, $headers, JSON_UNESCAPED_UNICODE);
    }
}
