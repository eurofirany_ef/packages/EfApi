<?php

namespace Eurofirany\EfApi\Http\Middleware;

use Closure;
use Eurofirany\EfApi\EfApi;
use Eurofirany\EfApi\Exceptions\ApiException;
use Illuminate\Http\Request;

class EfTokenMiddleware
{
    /**
     * @throws \App\Exceptions\ApiException
     */
    public function handle(Request $request, Closure $next)
    {
        $token = request()->bearerToken();

        if ($token && app(EfApi::class)->checkToken($token)) {
            return $next($request);
        }

        throw new ApiException('Unauthorized', 401);
    }
}
